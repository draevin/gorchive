package main

import (
	"archive/zip"
	"flag"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/djherbis/times"
)

const logFileDEF = "{yyyy-mm-dd}-archive.log"

func main() {
	sourcePath := flag.String("source", ".", "Directory from which to collect files; must be absolute and should end with /")
	destPath := flag.String("dest", ".", "Directory in which to place zipped files; must be absolute and should end with /")
	ignore := flag.String("ignore", "", "Wildcard match; matching directory names will be ignored")
	ageCutoff := flag.Int("age", 0, "Maximum age (in days) of files to leave in source")
	logger := flag.String("log", strings.Join([]string{"./", logFileDEF}, ""), "Directory in which to place the log file")
	delete := flag.Bool("d", false, "(D)elete objects from -source after zip")
	noLogFile := flag.Bool("c", false, "Disable logging to file; ignores logFile and logDir and outputs to (c)onsole")
	verbose := flag.Bool("v", false, "(V)erbose - Includes Trace and Debug logs")
	quiet := flag.Bool("q", false, "(Q)uiet all non-fatal logging")

	flag.Parse()

	var (
		files []string
		err   error
	)

	isLogFileDefault := *logger == strings.Join([]string{"./", logFileDEF}, "")

	logDir, logFile := filepath.Split(*logger)
	logDir, err = filepath.Abs(logDir)

	if err != nil {
		log.Fatal("Log directory path invalid. Ensure that the path exists!")
	}

	logFile, err = filepath.Abs(CheckLogFileSpec(logDir, logFile))
	if err != nil {
		log.Fatal("Logfile name invalid!")
	}

	if !*noLogFile {
		err = SetUpLogging(logFile)
		if err != nil {
			log.Fatal("Could not set up logging! Ensure selected log location ", logFile, " exists and current user has write permissions.")
		}
	} else if !isLogFileDefault {
		log.Warn("Custom logging location specified along with -noLogFile flag! Logging to standard output.")
	}

	if *verbose && *quiet {
		log.Warn("Cannot enable quiet and verbose output simultaneously. Outputting standard log level.")
	} else if *verbose {
		log.SetLevel(log.TraceLevel)
	} else if *quiet {
		log.SetLevel(log.FatalLevel)
	}

	*sourcePath, err = filepath.Abs(*sourcePath)
	_, err = os.Stat(*sourcePath)
	if err != nil {
		log.Fatal("Source path ", *sourcePath, " is invalid! Ensure that the path exists and current user has read permissions")
	}
	*destPath, err = filepath.Abs(*destPath)
	_, err = os.Stat(*destPath)
	if err != nil {
		log.Fatal("Destination path ", *destPath, " is invalid! Ensure that the path exists and current user has write permissions")
	}

	if *sourcePath == "" || *destPath == "" {
		log.Fatal("A source and destination path must be provided!")
	}

	if *ignore != "" {
		_, err = filepath.Match(*ignore, "")
		if err != nil {
			log.Fatal("Ignore pattern is malformed.")
		}
	}

	log.Info("Gathering files for filter: older than ", *ageCutoff, " days from ", *sourcePath)
	files, err = ReadSourceDir(*sourcePath, *ageCutoff, *ignore)
	if err != nil {
		log.Fatal(err)
	}

	archiveTime, err := RunArchiveJob(files, *destPath)
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Archival complete! Duration: ", archiveTime.String())

	if *delete {
		err = RemoveFiles(files)
		if err != nil {
			log.Fatal(err)
		}
	}
}

//RunArchiveJob - Run archive based on given source, dest, and age cutoff in days
func RunArchiveJob(items []string, dest string) (time.Duration, error) {
	start := time.Now()

	counter := 0
	if len(items) > 0 {
		totalCount := len(items)

		var dirPluralization string
		if len(items) == 1 {
			dirPluralization = " directory"
		} else {
			dirPluralization = " directories"
		}
		log.Info("Archiving ", len(items), dirPluralization, " to ", dest)

		const maxConcurrentRoutines = 4
		concurRoutines := make(chan struct{}, maxConcurrentRoutines)

		for i := 0; i < maxConcurrentRoutines; i++ {
			concurRoutines <- struct{}{}
		}

		done := make(chan bool)
		waiter := make(chan bool)
		var superErr error

		go func() {
			for i := 0; i < len(items); i++ {
				<-done
				concurRoutines <- struct{}{}
			}
			waiter <- true
		}()

		for _, item := range items {
			<-concurRoutines
			go func(item string) {
				_, itemName := filepath.Split(item)

				counter++
				log.Println("Targeting Source Item [", counter, "/", totalCount, "]", strings.TrimPrefix(itemName, "\\"))

				zipName := NormalizeZipExt(itemName)
				destFile := filepath.Join(dest, zipName)

				err := ZipDir(item, destFile)
				if err != nil {
					superErr = err
				}

				log.Trace("Created Archive:", destFile)
				done <- true
			}(item)
		}

		<-waiter

		if superErr != nil {
			return time.Since(start), superErr
		}
	} else {
		log.Info("No items found eligible for archival with given criteria! No action taken.")
	}

	return time.Since(start), nil
}

// ReadSourceDir - reads items from given directory
func ReadSourceDir(sourcePath string, ageCutoff int, ignorePattern string) ([]string, error) {

	var items []string
	cutoffDate := time.Now().AddDate(0, 0, -1*ageCutoff)

	f, err := os.Open(sourcePath)
	if err != nil {
		return items, err
	}

	itemInfo, err := f.Readdir(-1)
	f.Close()

	if err != nil {
		return items, err
	}

	for _, item := range itemInfo {
		fullPath := filepath.Join(sourcePath, item.Name())

		times, err := times.Stat(fullPath)
		if err != nil {
			return items, err
		}

		if item.IsDir() {
			isMatch, _ := filepath.Match(ignorePattern, item.Name())
			if !isMatch {
				if (times.HasChangeTime() && times.ChangeTime().Before(cutoffDate)) || (!times.HasChangeTime() && times.HasBirthTime() && times.BirthTime().Before(cutoffDate)) {
					items = append(items, fullPath)
				}
			}
		}
	}

	return items, nil
}

//ZipDir modified from gist svett/compress.go
func ZipDir(source, target string) error {
	zipfile, err := os.Create(target)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		path = filepath.Clean(path)
		source := strings.Join([]string{filepath.Clean(source), string(filepath.Separator)}, "")
		header.Name = strings.TrimPrefix(path, source)

		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		//ignore . (Linux)
		if header.Name == source {
			return nil
		}

		writer, err := archive.CreateHeader(header)

		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(writer, file)
		return err
	})

	return err
}

//NormalizeZipExt - check for .zip, add if missing
func NormalizeZipExt(s string) string {
	if !strings.HasSuffix(s, ".zip") {
		s = strings.Join([]string{s, "zip"}, ".")
	}

	return s
}

//RemoveFiles - remove all files present in list
func RemoveFiles(files []string) error {
	log.Info("Removing all archived dirs and contents.")
	for _, file := range files {
		err := os.RemoveAll(file)
		if err != nil {
			return err
		}

		log.Trace("Removed Source Directory:", file)
	}
	return nil
}

//CheckLogFileSpec - check if log file specified, set default otherwise
func CheckLogFileSpec(logDir, logFile string) string {
	if logFile == logFileDEF || logFile == "" {
		today := time.Now().Format("2006-01-02")
		logFileName := strings.Join([]string{today, "archive.log"}, "-")
		logFile = filepath.Join(logDir, logFileName)
	}

	return logFile
}

//SetUpLogging - direct logs to given file
func SetUpLogging(logFile string) error {
	f, err := os.OpenFile(logFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	log.SetOutput(f)
	log.Info("Logfile established!")

	return nil
}
